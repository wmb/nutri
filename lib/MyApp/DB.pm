package MyApp::DB;

use 5.026;
use strict;
use warnings ( FATAL => 'all' );

# Use UTF-8 encoding
use utf8;

# Use croak/carp instead of die/warn
# (cf. Effective Perl Programming, item 102)
use Carp;

# DBI
use DBI qw(:sql_types);
use DBD::SQLite::Constants qw(:file_open);

# JSON
use JSON::MaybeXS;

# Satisfy perlcritic
use Readonly;
use English '-no_match_vars';

Readonly our $VERSION => '0.0.1';

Readonly my $DBD_DRIVER => $ENV{'DBD_DRIVER'} || 'SQLite';
Readonly my $DB_NAME    => 'nutri';
Readonly my $DB_FILE    => "db/$DB_NAME.sqlite";

Readonly my $PRODUCTSTMT => <<'STMT';
    SELECT "code", "nom", "description", "codebarre"
    FROM "produit" WHERE "nom" LIKE ?
    ORDER BY "code" DESC
STMT

Readonly my $PRODUCTADDDDSTMT =>
  'INSERT INTO "produit" ("nom", "description", "codebarre") VALUES (?, ?, ?)';

Readonly my $USERSTMT =>
'SELECT "pseudo", "email", "motdepasse", "nom" FROM "membre" WHERE "pseudo" = ?';

Readonly my $USERADDSTMT =>
'INSERT INTO "membre" ("pseudo", "motdepasse", "email", "nom") VALUES (?, ?, ?, ?)';

Readonly my $CATPRODADDSTMT =>
  'INSERT INTO "categorie_prod" ("nom", "description") VALUES (?, ?)';

Readonly my $CATINGREDADDSTMT =>
  'INSERT INTO "categorie_ingred" ("nom", "description") VALUES (?, ?)';

Readonly my $CATPRODGETSTMT =>
  'SELECT "nom", "description" FROM "categorie_prod" WHERE "nom" LIKE ?';

Readonly my $CATPRODGETONESTMT =>
  'SELECT "nom", "description" FROM "categorie_prod" WHERE "nom" = ?';

Readonly my @PRODSBYCATSTMT => (
    'SELECT '
      . join(
        q(, ) => map { '"produit".' . join $_ => qw( " " ) }
          qw( code nom description codebarre )
      )
      . ', "categorie_prod"."nom" '
      . 'FROM "produit" '
      . 'JOIN "prod_catp" '
      . 'ON "produit"."code" = "prod_catp"."produit" '
      . 'JOIN "categorie_prod" '
      . 'ON "prod_catp"."categorie" = "categorie_prod"."nom" '
      . 'WHERE "prod_catp"."categorie" = ?',
    'SELECT '
      . join(
        q(, ) => map { '"produit".' . join $_ => qw( " " ) }
          qw( code nom description codebarre )
      )
      . ', "categorie_prod"."nom" '
      . 'FROM "produit" '
      . 'JOIN "prod_catp" '
      . 'ON "produit"."code" = "prod_catp"."produit" '
      . 'JOIN "categorie_prod" '
      . 'ON "prod_catp"."categorie" = "categorie_prod"."nom" '
      . 'WHERE "prod_catp"."categorie" LIKE ?'
);

Readonly my $CATEGORYSFORPRODSTMT => 'SELECT "c"."nom", "c"."description"'
  . ' FROM "categorie_prod" AS "c"'
  . ' JOIN "prod_catp" AS "p"'
  . ' ON "c"."nom" = "p"."categorie"'
  . ' WHERE "p"."produit" = CAST (? AS INTEGER)';

Readonly my $PRODBYBARCODESTMT =>
  'SELECT "code", "nom", "description", "codebarre"'
  . ' FROM "produit"'
  . ' WHERE "codebarre" = ?';

Readonly my %LISTCATEGORIESSTMT => (
    product    => 'SELECT "nom", "description" FROM "categorie_prod"',
    ingredient => 'SELECT "nom", "description" FROM "categorie_ingred"'
);

Readonly my $LISTNGREDIENTSTMT =>
  'SELECT "nom", "description", "categorie" FROM "ingredient"';

Readonly my $ADDPRODUCTSTMT =>
  'INSERT INTO "produit" ("nom", "description", "codebarre") VALUES (?, ?, ?)';

Readonly my $ADDPRODADDCATSTMT =>
  'INSERT INTO "prod_catp" ("produit", "categorie") VALUES (?, ?)';

Readonly my $ADDPRODADDINGREDTMT =>
'INSERT INTO "prod_contient_ingred" ("produit", "ingredient", "taux") VALUES (?, ?, CAST (? AS REAL))';

Readonly my $ADDPRODADDREALINGREDTMT =>
'INSERT INTO "prod_contient_ingred" ("produit", "ingredient", "taux") VALUES (?, ?, NULL)';

Readonly my $CREATEINGREDIENTSTMT =>
'INSERT INTO "ingredient" ("nom", "description", "categorie") VALUES (?, ?, ?)';

Readonly my $GETTAUXBYINGREDCATEGORYSTMT => <<'STMT';
        SELECT "prod_contient_ingred"."taux"
        FROM   "prod_contient_ingred"
        JOIN   "ingredient"
        ON     "prod_contient_ingred"."ingredient" = "ingredient"."nom"
        WHERE  "prod_contient_ingred"."produit" = ?
        AND    "ingredient"."categorie" = ?
STMT

Readonly my $GETTAUXBYINGREDNAMESTMT => <<'STMT';
        SELECT "taux"
        FROM   "prod_contient_ingred"
        WHERE  "produit" = ?
        AND    "ingredient" = ?
STMT

Readonly my $INGREDIENTSFROMPRODUCT => <<'STMT';
        SELECT "ingredient"."nom", "ingredient"."description", "ingredient"."categorie",
            "prod_contient_ingred"."taux"
        FROM   "ingredient"
        JOIN   "prod_contient_ingred"
        ON     "ingredient"."nom" = "prod_contient_ingred"."ingredient"
        WHERE  "prod_contient_ingred"."produit" = ?
        AND    "ingredient"."categorie" != 'Additive'
STMT

Readonly my $ADDITIVESFROMPRODUCT => <<'STMT';
        SELECT "ingredient"."nom", "ingredient"."description",
            "prod_contient_ingred"."taux"
        FROM   "ingredient"
        JOIN   "prod_contient_ingred"
        ON     "ingredient"."nom" = "prod_contient_ingred"."ingredient"
        WHERE  "prod_contient_ingred"."produit" = ?
        AND    "ingredient"."categorie" = 'Additive'
STMT

Readonly my $CATEGORIESFROMPRODUCT => <<'STMT';
        SELECT "categorie_prod"."nom", "categorie_prod"."description"
        FROM   "categorie_prod"
        JOIN   "prod_catp"
        ON     "categorie_prod"."nom" = "prod_catp"."categorie"
        WHERE  "prod_catp"."produit" = ?
STMT

Readonly my $ISSTARREDSTMT => <<'STMT';
        SELECT *
        FROM   "favoris"
        WHERE  "favoris"."membre" = ?
        AND    "favoris"."produit" = ?
STMT

Readonly my $STARPRODUCTSTMT => <<'STMT';
        INSERT INTO "favoris" ("membre", "produit")
        VALUES (?, ?)
STMT

Readonly my $UNSTARPRODUCTSTMT => <<'STMT';
        DELETE FROM "favoris"
        WHERE  "membre"  = ?
        AND    "produit" = ?
STMT

Readonly my $LISTSTARREDPRODUCTSSTMT => <<'STMT';
        SELECT "produit"."code", "produit"."nom", "produit"."description",
            "produit"."codebarre"
        FROM    "produit"
        JOIN    "favoris"
        ON      "produit"."code" = "favoris"."produit"
        WHERE   "favoris"."membre" = ?
STMT

Readonly my $REPORTPRODUCTSTMT => <<'STMT';
        INSERT INTO "reports" ("membre", "produit", "titre", "description")
        VALUES (?, ?, ?, ?)
STMT

Readonly my $DELETEHISTORYSTMT => <<'STMT';
        DELETE FROM "history"
        WHERE "membre"  = ?
        AND   "produit" = ?
STMT
Readonly my $ADDHISTORYSTMT => <<'STMT';
        INSERT INTO "history" ("membre", "produit")
        VALUES (?, ?)
STMT

Readonly my $GETHISTORYSTMT => <<'STMT';
        SELECT "produit"."code", "produit"."nom", "produit"."description",
            "produit"."codebarre"
        FROM    "produit"
        JOIN    "history"
        ON      "produit"."code" = "history"."produit"
        WHERE   "history"."membre" = ?
STMT

Readonly my %ADDITIVES_TYPES => (
    E100     => 4,
    E100i    => 4,
    E100ii   => 4,
    E101     => 4,
    E101i    => 4,
    E101ii   => 4,
    E101iii  => 4,
    E130     => 4,
    E140     => 4,
    E141     => 4,
    E141i    => 4,
    E141ii   => 4,
    E150a    => 4,
    E150c    => 4,
    E153     => 4,
    E160c    => 4,
    E160d    => 4,
    E160di   => 4,
    E160dii  => 4,
    E160diii => 4,
    E160e    => 4,
    E160f    => 4,
    E161     => 4,
    E161a    => 4,
    E161b    => 4,
    E161bi   => 4,
    E161bii  => 4,
    E161c    => 4,
    E161d    => 4,
    E161e    => 4,
    E161f    => 4,
    E161j    => 4,
    E162     => 4,
    E163     => 4,
    E163ii   => 4,
    E163iii  => 4,
    E163iv   => 4,
    E163v    => 4,
    E164     => 4,
    E165     => 4,
    E166     => 4,
    E170     => 4,
    E170i    => 4,
    E170ii   => 4,
    E172     => 4,
    E172i    => 4,
    E172ii   => 4,
    E172iii  => 4,
    E175     => 4,
    E180     => 4,
    E182     => 4,
    E200     => 4,
    E201     => 4,
    E202     => 4,
    E203     => 4,
    E234     => 4,
    E262     => 4,
    E262i    => 4,
    E262ii   => 4,
    E263     => 4,
    E270     => 4,
    E290     => 4,
    E297     => 4,
    E300     => 4,
    E301     => 4,
    E302     => 4,
    E303     => 4,
    E304     => 4,
    E305     => 4,
    E306     => 4,
    E307     => 4,
    E307a    => 4,
    E307b    => 4,
    E307c    => 4,
    E308     => 4,
    E309     => 4,
    E311     => 4,
    E312     => 4,
    E313     => 4,
    E314     => 4,
    E315     => 4,
    E316     => 4,
    E317     => 4,
    E318     => 4,
    E322     => 4,
    E322i    => 4,
    E322ii   => 4,
    E323     => 4,
    E325     => 4,
    E326     => 4,
    E327     => 4,
    E328     => 4,
    E329     => 4,
    E331     => 4,
    E331i    => 4,
    E331ii   => 4,
    E331iii  => 4,
    E332     => 4,
    E332i    => 4,
    E332ii   => 4,
    E333     => 4,
    E333i    => 4,
    E333ii   => 4,
    E333iii  => 4,
    E334     => 4,
    E335     => 4,
    E335i    => 4,
    E335ii   => 4,
    E336     => 4,
    E336i    => 4,
    E336ii   => 4,
    E337     => 4,
    E339     => 4,
    E339i    => 4,
    E339ii   => 4,
    E339iii  => 4,
    E340     => 4,
    E340i    => 4,
    E340ii   => 4,
    E340iii  => 4,
    E341     => 4,
    E341i    => 4,
    E341ii   => 4,
    E341iii  => 4,
    E342     => 4,
    E342i    => 4,
    E342ii   => 4,
    E343     => 4,
    E343i    => 4,
    E343ii   => 4,
    E343iii  => 4,
    E344     => 4,
    E345     => 4,
    E349     => 4,
    E350     => 4,
    E350i    => 4,
    E350ii   => 4,
    E351     => 4,
    E351i    => 4,
    E351ii   => 4,
    E352     => 4,
    E352i    => 4,
    E352ii   => 4,
    E353     => 4,
    E354     => 4,
    E356     => 4,
    E357     => 4,
    E359     => 4,
    E363     => 4,
    E364     => 4,
    E364i    => 4,
    E364ii   => 4,
    E365     => 4,
    E366     => 4,
    E367     => 4,
    E368     => 4,
    E370     => 4,
    E380     => 4,
    E381     => 4,
    E383     => 4,
    E384     => 4,
    E387     => 4,
    E388     => 4,
    E389     => 4,
    E390     => 4,
    E391     => 4,
    E399     => 4,
    E400     => 4,
    E401     => 4,
    E402     => 4,
    E403     => 4,
    E404     => 4,
    E408     => 4,
    E410     => 4,
    E414     => 4,
    E417     => 4,
    E418     => 4,
    E419     => 4,
    E424     => 4,
    E426     => 4,
    E427     => 4,
    E428     => 4,
    E429     => 4,
    E440     => 4,
    E440a    => 4,
    E440b    => 4,
    E441     => 4,
    E442     => 4,
    E444     => 4,
    E446     => 4,
    E450     => 4,
    E450viii => 4,
    E451     => 4,
    E451i    => 4,
    E452     => 4,
    E452v    => 4,
    E452vi   => 4,
    E457     => 4,
    E458     => 4,
    E459     => 4,
    E467     => 4,
    E470     => 4,
    E471     => 4,
    E472a    => 4,
    E472b    => 4,
    E472c    => 4,
    E472d    => 4,
    E472e    => 4,
    E472f    => 4,
    E472g    => 4,
    E473     => 4,
    E473a    => 4,
    E474     => 4,
    E475     => 4,
    E479     => 4,
    E479b    => 4,
    E481     => 4,
    E481i    => 4,
    E481ii   => 4,
    E482     => 4,
    E482i    => 4,
    E482ii   => 4,
    E483     => 4,
    E484     => 4,
    E485     => 4,
    E486     => 4,
    E488     => 4,
    E489     => 4,
    E491     => 4,
    E492     => 4,
    E493     => 4,
    E494     => 4,
    E495     => 4,
    E496     => 4,
    E500     => 4,
    E500i    => 4,
    E500ii   => 4,
    E500iii  => 4,
    E501     => 4,
    E501i    => 4,
    E501ii   => 4,
    E502     => 4,
    E503     => 4,
    E503i    => 4,
    E503ii   => 4,
    E504     => 4,
    E504i    => 4,
    E504ii   => 4,
    E505     => 4,
    E512     => 4,
    E535     => 4,
    E536     => 4,
    E537     => 4,
    E538     => 4,
    E539     => 4,
    E540     => 4,
    E542     => 4,
    E544     => 4,
    E545     => 4,
    E546     => 4,
    E550     => 4,
    E550i    => 4,
    E550ii   => 4,
    E552     => 4,
    E553     => 4,
    E553i    => 4,
    E553ii   => 4,
    E570     => 4,
    E572     => 4,
    E575     => 4,
    E576     => 4,
    E577     => 4,
    E578     => 4,
    E579     => 4,
    E580     => 4,
    E585     => 4,
    E586     => 4,
    E630     => 4,
    E631     => 4,
    E632     => 4,
    E633     => 4,
    E634     => 4,
    E635     => 4,
    E637     => 4,
    E638     => 4,
    E639     => 4,
    E640     => 4,
    E641     => 4,
    E642     => 4,
    E650     => 4,
    E900     => 4,
    E900a    => 4,
    E900b    => 4,
    E901     => 4,
    E902     => 4,
    E903     => 4,
    E904     => 4,
    E906     => 4,
    E907     => 4,
    E908     => 4,
    E909     => 4,
    E910     => 4,
    E911     => 4,
    E912     => 4,
    E913     => 4,
    E915     => 4,
    E916     => 4,
    E918     => 4,
    E919     => 4,
    E920     => 4,
    E922     => 4,
    E923     => 4,
    E927     => 4,
    E927b    => 4,
    E928     => 4,
    E929     => 4,
    E930     => 4,
    E938     => 4,
    E939     => 4,
    E941     => 4,
    E945     => 4,
    E946     => 4,
    E948     => 4,
    E949     => 4,
    E955     => 4,
    E956     => 4,
    E957     => 4,
    E959     => 4,
    E960     => 4,
    E961     => 4,
    E963     => 4,
    E964     => 4,
    E968     => 4,
    E999     => 4,
    E999i    => 4,
    E999ii   => 4,
    E1000    => 4,
    E1001    => 4,
    E1001i   => 4,
    E1001ii  => 4,
    E1001iii => 4,
    E1001iv  => 4,
    E1001v   => 4,
    E1001vi  => 4,
    E1100    => 4,
    E1101    => 4,
    E1101i   => 4,
    E1101ii  => 4,
    E1101iii => 4,
    E1101iv  => 4,
    E1102    => 4,
    E1103    => 4,
    E1104    => 4,
    E1202    => 4,
    E1203    => 4,
    E1204    => 4,
    E1505    => 4,
    E1517    => 4,
    E101a    => 4,
    E161g    => 4,
    E161h    => 4,
    E161hi   => 4,
    E161hii  => 4,
    E161i    => 4,
    E174     => 4,
    E181     => 4,
    E235     => 4,
    E264     => 4,
    E330     => 4,
    E355     => 4,
    E405     => 4,
    E406     => 4,
    E409     => 4,
    E412     => 4,
    E413     => 4,
    E415     => 4,
    E416     => 4,
    E420     => 4,
    E420i    => 4,
    E420ii   => 4,
    E425     => 4,
    E445     => 4,
    E460     => 4,
    E460i    => 4,
    E460ii   => 4,
    E461     => 4,
    E462     => 4,
    E463     => 4,
    E464     => 4,
    E465     => 4,
    E466     => 4,
    E468     => 4,
    E469     => 4,
    E470i    => 4,
    E470ii   => 4,
    E477     => 4,
    E478     => 4,
    E480     => 4,
    E487     => 4,
    E507     => 4,
    E508     => 4,
    E509     => 4,
    E511     => 4,
    E514     => 4,
    E515     => 4,
    E516     => 4,
    E517     => 4,
    E518     => 4,
    E524     => 4,
    E525     => 4,
    E526     => 4,
    E527     => 4,
    E528     => 4,
    E529     => 4,
    E530     => 4,
    E541     => 4,
    E541i    => 4,
    E541ii   => 4,
    E551     => 4,
    E553iii  => 4,
    E554     => 4,
    E555     => 4,
    E556     => 4,
    E557     => 4,
    E558     => 4,
    E560     => 4,
    E574     => 4,
    E636     => 4,
    E905c    => 4,
    E905ci   => 4,
    E905cii  => 4,
    E917     => 4,
    E921     => 4,
    E942     => 4,
    E943a    => 4,
    E943b    => 4,
    E944     => 4,
    E953     => 4,
    E958     => 4,
    E965     => 4,
    E965i    => 4,
    E965ii   => 4,
    E966     => 4,
    E967     => 4,
    E1200    => 4,
    E1518    => 4,

    E160a    => 3,
    E160ai   => 3,
    E160aii  => 3,
    E160aiii => 3,
    E160aiv  => 3,
    E233     => 3,
    E241     => 3,
    E243     => 3,
    E260     => 3,
    E261     => 3,
    E261i    => 3,
    E261ii   => 3,
    E265     => 3,
    E266     => 3,
    E280     => 3,
    E281     => 3,
    E282     => 3,
    E283     => 3,
    E296     => 3,
    E324     => 3,
    E338     => 3,
    E375     => 3,
    E407     => 3,
    E407a    => 3,
    E510     => 3,
    E905     => 3,
    E905a    => 3,
    E905b    => 3,
    E914     => 3,
    E102     => 3,
    E107     => 3,
    E120     => 3,
    E121     => 3,
    E122     => 3,
    E125     => 3,
    E126     => 3,
    E129     => 3,
    E131     => 3,
    E132     => 3,
    E133     => 3,
    E142     => 3,
    E150b    => 3,
    E150d    => 3,
    E151     => 3,
    E154     => 3,
    E155     => 3,
    E160b    => 3,
    E160bi   => 3,
    E160bii  => 3,
    E171     => 3,
    E209     => 3,
    E210     => 3,
    E211     => 3,
    E212     => 3,
    E213     => 3,
    E214     => 3,
    E215     => 3,
    E216     => 3,
    E217     => 3,
    E218     => 3,
    E219     => 3,
    E220     => 3,
    E221     => 3,
    E222     => 3,
    E223     => 3,
    E224     => 3,
    E225     => 3,
    E226     => 3,
    E227     => 3,
    E228     => 3,
    E239     => 3,
    E242     => 3,
    E251     => 3,
    E252     => 3,
    E284     => 3,
    E285     => 3,
    E321     => 3,
    E411     => 3,
    E421     => 3,
    E422     => 3,
    E430     => 3,
    E431     => 3,
    E432     => 3,
    E433     => 3,
    E434     => 3,
    E435     => 3,
    E436     => 3,
    E450i    => 3,
    E450ii   => 3,
    E450iii  => 3,
    E450iv   => 3,
    E450v    => 3,
    E450vi   => 3,
    E450vii  => 3,
    E451ii   => 3,
    E452i    => 3,
    E452ii   => 3,
    E452iii  => 3,
    E452iv   => 3,
    E476     => 3,
    E513     => 3,
    E620     => 3,
    E621     => 3,
    E622     => 3,
    E623     => 3,
    E624     => 3,
    E625     => 3,
    E626     => 3,
    E627     => 3,
    E628     => 3,
    E629     => 3,
    E905d    => 3,
    E905e    => 3,
    E905f    => 3,
    E905g    => 3,
    E927a    => 3,
    E940     => 3,
    E950     => 3,
    E954     => 3,
    E954i    => 3,
    E954ii   => 3,
    E954iii  => 3,
    E954iv   => 3,
    E962     => 3,
    E1105    => 3,
    E1201    => 3,
    E1400    => 3,
    E1401    => 3,
    E1402    => 3,
    E1403    => 3,
    E1404    => 3,
    E1405    => 3,
    E1410    => 3,
    E1411    => 3,
    E1412    => 3,
    E1413    => 3,
    E1414    => 3,
    E1420    => 3,
    E1421    => 3,
    E1422    => 3,
    E1423    => 3,
    E1440    => 3,
    E1442    => 3,
    E1443    => 3,
    E1450    => 3,
    E1451    => 3,
    E1452    => 3,
    E1503    => 3,
    E1519    => 3,
    E1520    => 3,
    E1521    => 3,

    E104    => 2,
    E110    => 2,
    E123    => 2,
    E127    => 2,
    E152    => 2,
    E230    => 2,
    E231    => 2,
    E232    => 2,
    E236    => 2,
    E237    => 2,
    E238    => 2,
    E249    => 2,
    E250    => 2,
    E310    => 2,
    E319    => 2,
    E320    => 2,
    E385    => 2,
    E443    => 2,
    E519    => 2,
    E520    => 2,
    E521    => 2,
    E522    => 2,
    E523    => 2,
    E559    => 2,
    E925    => 2,
    E951    => 2,
    E103    => 2,
    E111    => 2,
    E124    => 2,
    E128    => 2,
    E173    => 2,
    E952    => 2,
    E952i   => 2,
    E952ii  => 2,
    E952iii => 2,
    E952iv  => 2,
    E143    => 2,
    E240    => 2,
    E386    => 2,
    E924    => 2,
    E924a   => 2,
    E924b   => 2,
    E926    => 2,
);

my $dbh;
my $json = undef;

sub new {
    my $class = shift;
    my $self  = {};
    $json = shift // { error => { error => JSON->false() } };

    return bless $self, $class;
}

sub dberror {
    my ( $self, $msg ) = @_;

    $json->{error}->{error} = JSON->true();
    $json->{error}->{msg}   = $msg // q(An error occurred.);

    return;
}

sub dbgetdsn {
    my $self = shift;
    return "dbi:$DBD_DRIVER:dbname=$DB_FILE";
}

sub dbconnect {
    my $self     = shift;
    my $readonly = shift // 0;

    my $dsn = $self->dbgetdsn;

    my $options = {
        AutoCommit => 1,
        RaiseError => 1,
        PrintError => 0,
    };

    if ($readonly) {
        $options->{sqlite_open_flags} = SQLITE_OPEN_READONLY;
    }

    $dbh = eval { DBI->connect( $dsn, q(), q(), $options ); };
    if ( $EVAL_ERROR || !defined $dbh ) {
        $self->dberror( 'DBI connect failed: ' . DBI::errstr() );
        return 0;
    }

    # Enable foreign keys
    if ( !$dbh->do('PRAGMA foreign_keys = ON') ) {
        $self->dberror( 'DBI connec: failed to set PRAGMA foreign_keys = ON: '
              . $dbh->errstr() );
        return 0;
    }

    return 1;
}

sub dbdisconnect {
    my $self = shift;

    if ( !defined $dbh ) {
        $self->dberror( 'dbdisconnect(): $' . 'dbh is undefined.' );
        return 0;
    }

    if ( !$dbh->disconnect() ) {
        $dbh->dberror( 'DBI disconnect failed: ' . $dbh->errstr() );
        return 0;
    }
    return 1;
}

sub dbprodsearch {
    my ( $self, $search_term ) = @_;

    if ( !defined $search_term ) {
        $self->dberror('dbprodsearch(): No search terms given!');
        return 0;
    }
    $json->{search} = $search_term;

    if ( !defined $dbh ) {
        $self->dberror( 'dbprodsearch(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $sth = $dbh->prepare($PRODUCTSTMT);
    if ( !defined $sth ) {
        $self->dberror( 'dbprodsearch(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    if ( !$sth->execute( q(%) . $search_term . q(%) ) ) {
        $self->dberror( 'dbprodsearch(): execute() failed: ' . $sth->errstr() );
        return 0;
    }

    $sth->bind_columns( \my ( $code, $nom, $description, $codebarre ) );
    if ( $sth->err() ) {
        $self->dberror(
            'dbprodsearch(): bind_columns() failed: ' . $sth->errstr() );
        return 0;
    }

    my @results = ();
    while ( defined $sth->fetchrow_arrayref() ) {
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $nom,
                code        => $code,
                description => $description,
                codebarre   => $codebarre
            )
          };
    }
    $json->{nresults} = @results;

    if ( $sth->err() ) {
        $self->dberror( 'fetchrow_arrayref() failed: ' . $sth->errstr() );
        return 0;
    }

    $json->{results} = \@results;

    return 1;
}

sub getjson {
    my $self = shift;
    return $json;
}

sub finduser {
    my ( $self, $user ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'finduser(): $' . 'dbh is undefined.' );
        return;
    }

    my $sth = $dbh->prepare($USERSTMT);
    if ( !defined $sth ) {
        $self->dberror( 'finduser(): prepare() failed: ' . $dbh->errstr() );
        return;
    }

    if ( !$sth->execute($user) ) {
        $self->dberror( 'finduser(): execute() failed: ' . $sth->errstr() );
        return;
    }

    $sth->bind_columns( \my ( $pseudo, $email, $motdepasse, $nom ) );
    if ( $sth->err() ) {
        $self->dberror(
            'finduser(): bind_columns() failed: ' . $sth->errstr() );
        return;
    }

    my $userh;
    if ( defined $sth->fetchrow_arrayref() ) {
        $userh = {
            map { Encode::decode( 'UTF-8', $_ ) } (
                pseudo => $pseudo,
                email  => $email,
                pass   => $motdepasse,
                nom    => $nom
            )
        };
    }
    $sth->finish;

    $sth->err() and return;
    return $userh;
}

sub createuser {
    my ( $self, $pseudo, $motdepasse, $email, $nom ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'createuser(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $sth = $dbh->prepare($USERADDSTMT);
    if ( !defined $sth ) {
        $self->dberror( 'createuser(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    foreach my $param ( $pseudo, $motdepasse, $email, $nom ) {
        $sth->bind_param( ++$p_num, $param, SQL_VARCHAR );
        if ( $sth->err() ) {
            $self->dberror(
                'createuser(): bind_columns() failed: ' . $sth->errstr() );
            return 0;
        }
    }

    $sth->execute();
    if ( $sth->err() ) {
        $self->dberror( 'createuser(): execute() failed: ' . $sth->errstr() );
        return 0;
    }

    return 1;
}

sub addcatprod {
    my ( $self, $name, $desc ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'addcatprod(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $sth = $dbh->prepare($CATPRODADDSTMT);
    if ( !defined $sth ) {
        $self->dberror( 'addcatprod(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    foreach my $param ( $name, $desc ) {
        $sth->bind_param( ++$p_num, $param, SQL_VARCHAR );
        if ( $sth->err ) {
            $self->dberror(
                'addcatprod(): bind_columns() failed: ' . $sth->errstr() );
            return 0;
        }
    }

    my $rv = eval { $sth->execute };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror( 'addcatprod(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    return 1;
}

sub addcatingred {
    my ( $self, $name, $desc ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'addcatingred(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $sth = $dbh->prepare($CATINGREDADDSTMT);
    if ( !defined $sth ) {
        $self->dberror( 'addcatingred(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    foreach my $param ( $name, $desc ) {
        $sth->bind_param( ++$p_num, $param, SQL_VARCHAR );
        if ( $sth->err ) {
            $self->dberror(
                'addcatingred(): bind_columns() failed: ' . $sth->errstr() );
            return 0;
        }
    }

    my $rv = eval { $sth->execute };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror( 'addcatingred(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    return 1;
}

sub getcatprods {
    my ( $self, $name ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'getcatprods(): $' . 'dbh is undefined.' );
        return 0;
    }

    my @results;
    my $rows = $dbh->selectall_arrayref( $CATPRODGETSTMT, { RaiseError => 1 },
        join $name, qw( % % ) );

    if ( $dbh->err ) {
        $self->dberror(
            'getcatprods(): selectall_arrayref failed: ' . $dbh->errstr );
        return 0;
    }

    foreach my $row ( @{$rows} ) {
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $row->[0],
                description => $row->[1]
            )
          };
    }

    $json->{results}  = \@results;
    $json->{nresults} = @results;

    return 1;
}

sub getcatprod {
    my ( $self, $name ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'getcatprod(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $category;
    if (
        defined(
            my $row = $dbh->selectrow_arrayref(
                $CATPRODGETSTMT, { RaiseError => 1 }, $name
            )
        )
      )
    {
        $category = {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $row->[0],
                description => $row->[1]
            )
        };
    }

    if ( $dbh->err ) {
        $self->dberror(
            'getcatprod(): selectrow_arrayref failed: ' . $dbh->errstr );
        return 0;
    }

    $json->{results} = $category // undef;
    $json->{found} = defined $category ? JSON->true : JSON->false;

    return 1;
}

sub products_by_catprod {
    my ( $self, $name, $exact ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'products_by_catprod(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $stmt = $PRODSBYCATSTMT[ $exact ? 0 : 1 ];

    my $rows = $dbh->selectall_arrayref(
        $stmt,
        { RaiseError => 1 },
        $exact ? $name : join $name,
        qw( % % )
    );

    if ( $dbh->err || !$rows ) {
        $self->dberror( 'products_by_catprod(): selectall_arrayref failed: '
              . $dbh->errstr );
        return 0;
    }

    my @results;
    foreach my $row ( @{$rows} ) {
        my ( $nom, $code, $description, $codebarre, $categorie ) = @{$row};
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $nom,
                code        => $code,
                description => $description,
                codebarre   => $codebarre,
                categorie   => $categorie
            )
          };
    }

    $json->{nresults}      = @results;
    $json->{results}       = \@results;
    $json->{search}        = $name;
    $json->{strict_search} = $exact ? JSON->true : JSON->false;

    return 1;
}

#sub product_categories {
#    my ( $self, $code ) = @_;
#
#    if ( !defined $dbh ) {
#        $self->dberror( 'product_categories(): $' . 'dbh is undefined.' );
#        return 0;
#    }
#
#    my $rows =
#      $dbh->selectall_arrayref( $CATEGORYSFORPRODSTMT, { RaiseError => 1 },
#        $code );
#
#    if ( $dbh->err || !$rows ) {
#        $self->dberror( 'product_categories(): selectall_arrayref failed: '
#              . $dbh->errstr );
#        return 0;
#    }
#
#    my @results;
#    foreach my $row ( @{$rows} ) {
#        push @results,
#          {
#            map { Encode::decode( 'UTF-8', $_ ) } (
#                nom         => $row->[0],
#                description => $row->[1]
#            )
#          };
#    }
#    $json->{nresults} = @results;
#    $json->{results}  = \@results;
#
#    return 1;
#}

sub prod_by_barcode {
    my ( $self, $type, $code ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'prod_by_barcode(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $row = $dbh->selectrow_arrayref( $PRODBYBARCODESTMT, { RaiseError => 1 },
        "$type:$code" );

    if ( $dbh->err ) {
        $self->dberror(
            'prod_by_barcode(): selectrow_arrayref failed: ' . $dbh->errstr );
        return 0;
    }

    my @results;
    if ($row) {
        my ( $code_, $nom, $description, $codebarre ) = @{$row};
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $nom,
                code        => $code_,
                description => $description,
                codebarre   => $codebarre,
            )
          };
    }

    $json->{nresults} = @results;
    $json->{results}  = \@results;

    return 1;
}

sub listcategories {
    my ( $self, $category_type ) = @_;

    my @results;
    foreach my $row (
        @{
            $dbh->selectall_arrayref( $LISTCATEGORIESSTMT{$category_type},
                { RaiseError => 1 } )
        }
      )
    {
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $row->[0],
                description => $row->[1]
            )
          };
    }

    $dbh->err
      and croak( 'listcategories: selectall_arrayref failed: ' . $dbh->errstr );

    $json->{nresults} = @results;
    $json->{results}  = \@results;

    return 1;
}

sub listingredients {
    my ($self) = @_;

    my @results;
    foreach my $row (
        @{ $dbh->selectall_arrayref( $LISTNGREDIENTSTMT, { RaiseError => 1 } ) }
      )
    {
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $row->[0],
                description => $row->[1],
                categorie   => $row->[2]
            )
          };
    }

    $dbh->err
      and croak( 'listcategories: selectall_arrayref failed: ' . $dbh->errstr );

    $json->{nresults} = @results;
    $json->{results}  = \@results;

    return 1;
}

sub addproduct {
    my ( $self, $product ) = @_;

    my $sth = $dbh->prepare($ADDPRODUCTSTMT);
    if ( !defined $sth ) {
        $self->dberror( 'addproduct(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    foreach my $param ( $product->{nom}, $product->{description},
        $product->{codebarre} )
    {
        $sth->bind_param( ++$p_num, $param, SQL_VARCHAR );
        if ( $sth->err ) {
            $self->dberror(
                'addproduct(): bind_columns() failed: ' . $sth->errstr() );
            return 0;
        }
    }

    my $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror( 'addproduct(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    $json->{code} = $dbh->last_insert_id( (q()) x 4 ) // 'undefined';

    return 1;
}

sub add_product_category {
    my ( $self, $product, $category ) = @_;

    my $sth = $dbh->prepare($ADDPRODADDCATSTMT);
    if ( !defined $sth ) {
        $self->dberror(
            'add_product_category(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    $sth->bind_param( 1, 0 + $product, SQL_INTEGER );
    $sth->bind_param( 2, $category,    SQL_VARCHAR );
    if ( $sth->err ) {
        $self->dberror( 'add_product_category(): bind_columns() failed: '
              . $sth->errstr() );
        return 0;
    }

    my $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror(
            'add_product_category(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    $json->{code} = $product;

    return 1;
}

sub add_product_ingredient {
    my ( $self, $product, $ingredient, $taux ) = @_;

    if ($taux == -1) {
        return $self->add_product_real_ingredient( $product, $ingredient );
    }

    my $sth = $dbh->prepare($ADDPRODADDINGREDTMT);
    if ( !defined $sth ) {
        $self->dberror(
            'add_product_ingredient(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    $sth->bind_param( ++$p_num, 0 + $product, SQL_INTEGER );
    $sth->bind_param( ++$p_num, $ingredient,  SQL_VARCHAR );
    $sth->bind_param( ++$p_num, $taux,        SQL_REAL );
    if ( $sth->err ) {
        $self->dberror( 'add_product_ingredient(): bind_columns() failed: '
              . $sth->errstr() );
        return 0;
    }

    my $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror(
            'add_product_ingredient(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    $json->{code} = $product;

    return 1;
}

sub add_product_real_ingredient {
    my ( $self, $product, $ingredient ) = @_;

    my $sth = $dbh->prepare($ADDPRODADDREALINGREDTMT);
    if ( !defined $sth ) {
        $self->dberror(
            'add_product_real_ingredient(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    $sth->bind_param( ++$p_num, 0 + $product, SQL_INTEGER );
    $sth->bind_param( ++$p_num, $ingredient,  SQL_VARCHAR );
    if ( $sth->err ) {
        $self->dberror( 'add_product_real_ingredient(): bind_columns() failed: '
              . $sth->errstr() );
        return 0;
    }

    my $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror(
            'add_product_real_ingredient(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    $json->{code} = $product;

    return 1;
}


sub add_product_additive {
    my ( $self, $product, $additive ) = @_;

    my $sth = $dbh->prepare($ADDPRODADDINGREDTMT);
    if ( !defined $sth ) {
        $self->dberror(
            'add_product_additive(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    Readonly my %ADDITIVE_TYPES => (
        unknown   => 1,
        dangerous => 2,
        average   => 3,
        good      => 4,
    );

    my $taux;

    if ( exists $ADDITIVES_TYPES{$additive} ) {
        $taux = $ADDITIVES_TYPES{$additive};
    }
    else {
      ADDITIVE:
        for ($additive) {
            if (
m/(?: bad | dangerous | awful | crap ) \s+ (?: additive \s+ )? test/imosx
              )
            {
                $taux = $ADDITIVE_TYPES{dangerous};
                last ADDITIVE;
            }
            if (m/(?: average | okay ) \s+ (?: additive \s+ )? test/imosx) {
                $taux = $ADDITIVE_TYPES{average};
                last ADDITIVE;
            }
            if (
m/(?: good | pleasant | awesome ) \s+ (?: additive \s+ )? test/imosx
              )
            {
                $taux = $ADDITIVE_TYPES{good};
                last ADDITIVE;
            }
            $taux = $ADDITIVE_TYPES{unknown};
            last ADDITIVE;
        }
    }

    my $p_num;
    $sth->bind_param( ++$p_num, 0 + $product, SQL_INTEGER );
    $sth->bind_param( ++$p_num, $additive,    SQL_VARCHAR );
    $sth->bind_param( ++$p_num, $taux,        SQL_REAL );
    if ( $sth->err ) {
        $self->dberror( 'add_product_additive(): bind_columns() failed: '
              . $sth->errstr() );
        return 0;
    }

    my $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror(
            'add_product_additive(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    $json->{code} = $product;

    return 1;
}

sub create_ingredient {
    my ( $self, $name, $desc, $category ) = @_;

    my $sth = $dbh->prepare($CREATEINGREDIENTSTMT);
    if ( !defined $sth ) {
        $self->dberror(
            'create_ingredient(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    foreach my $param ( $name, $desc, $category ) {
        $sth->bind_param( ++$p_num, $param, SQL_VARCHAR );
        if ( $sth->err ) {
            $self->dberror( 'create_ingredient(): bind_columns() failed: '
                  . $sth->errstr() );
            return 0;
        }
    }

    my $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror(
            'create_ingredient(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    return 1;
}

sub product_ingredients {
    my ( $self, $code ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'product_ingredients(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $rows =
      $dbh->selectall_arrayref( $INGREDIENTSFROMPRODUCT, { RaiseError => 1 },
        $code );

    if ( $dbh->err ) {
        $self->dberror( 'product_ingredients(): selectrow_arrayref failed: '
              . $dbh->errstr );
        return 0;
    }

    my @results;
    foreach my $row ( @{$rows} ) {
        my ( $nom, $description, $categorie, $taux ) = @{$row};
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $nom,
                description => $description,
                categorie   => $categorie,
                taux        => $taux,
            )
          };
    }

    $json->{nresults} = @results;
    $json->{results}  = \@results;

    return 1;
}

sub product_additives {
    my ( $self, $code ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'product_additives(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $rows =
      $dbh->selectall_arrayref( $ADDITIVESFROMPRODUCT, { RaiseError => 1 },
        $code );

    if ( $dbh->err ) {
        $self->dberror(
            'product_additives(): selectrow_arrayref failed: ' . $dbh->errstr );
        return 0;
    }

    my @results;
    foreach my $row ( @{$rows} ) {
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $row->[0],
                description => $row->[1],
                type        => $row->[2],
            )
          };
    }

    $json->{nresults} = @results;
    $json->{results}  = \@results;

    return 1;
}

sub product_categories {
    my ( $self, $code ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'product_categories(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $rows =
      $dbh->selectall_arrayref( $CATEGORIESFROMPRODUCT, { RaiseError => 1 },
        $code );

    if ( $dbh->err ) {
        $self->dberror( 'product_categories(): selectrow_arrayref failed: '
              . $dbh->errstr );
        return 0;
    }

    my @results;
    foreach my $row ( @{$rows} ) {
        my ( $nom, $description ) = @{$row};
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $nom,
                description => $description,
            )
          };
    }

    $json->{nresults} = @results;
    $json->{results}  = \@results;

    return 1;
}

sub is_starred {
    my ( $self, $code, $user ) = @_;

    if ( !defined $dbh ) {
        $self->dberror( 'is_starred(): $' . 'dbh is undefined.' );
        return 0;
    }

    my $row = $dbh->selectrow_arrayref( $ISSTARREDSTMT, { RaiseError => 1 },
        $user, $code );

    if ( $dbh->err ) {
        $self->dberror(
            'is_starred(): selectrow_arrayref failed: ' . $dbh->errstr );
        return 0;
    }

    $json->{starred} = $row ? JSON->true() : JSON->false();

    return 1;
}

sub star_product {
    my ( $self, $code, $user ) = @_;

    my $sth = $dbh->prepare($STARPRODUCTSTMT);
    if ( !defined $sth ) {
        $self->dberror( 'star_product(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    my $params = [
        {
            param => $user,
            type  => SQL_VARCHAR,
        },
        {
            param => $code,
            type  => SQL_INTEGER,
        },
    ];
    foreach my $param ( @{$params} ) {
        $sth->bind_param( ++$p_num, $param->{param}, $param->{type} );
        if ( $sth->err ) {
            $self->dberror(
                'star_product(): bind_param() failed: ' . $sth->errstr() );
            return 0;
        }
    }

    my $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror( 'star_product(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    return 1;
}

sub unstar_product {
    my ( $self, $code, $user ) = @_;

    my $sth = $dbh->prepare($UNSTARPRODUCTSTMT);
    if ( !defined $sth ) {
        $self->dberror(
            'unstar_product(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    my $params = [
        {
            param => $user,
            type  => SQL_VARCHAR,
        },
        {
            param => $code,
            type  => SQL_INTEGER,
        },
    ];
    foreach my $param ( @{$params} ) {
        $sth->bind_param( ++$p_num, $param->{param}, $param->{type} );
        if ( $sth->err ) {
            $self->dberror(
                'unstar_product(): bind_param() failed: ' . $sth->errstr() );
            return 0;
        }
    }

    my $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror( 'unstar_product(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    return 1;
}

sub list_starred {
    my ( $self, $user ) = @_;

    my $rows =
      $dbh->selectall_arrayref( $LISTSTARREDPRODUCTSSTMT, { RaiseError => 1 },
        $user );

    if ( $dbh->err || !defined $rows ) {
        $self->dberror(
            'list_starred(): selectall_arrayref() failed: ' . $dbh->errstr() );
        return 0;
    }

    my @results;
    foreach my $row ( @{$rows} ) {
        my ( $code, $nom, $description, $codebarre ) = @{$row};
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                nom         => $nom,
                code        => $code,
                description => $description,
                codebarre   => $codebarre,
            )
          };
    }

    $json->{results}  = \@results;
    $json->{nresults} = @results;

    return 1;
}

sub report_product {
    my ( $self, $user, $code, $title, $description ) = @_;

    my $sth = $dbh->prepare($REPORTPRODUCTSTMT);
    if ( !defined $sth ) {
        $self->dberror( 'report_product(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    my $params = [
        {
            param => $user,
            type  => SQL_VARCHAR,
        },
        {
            param => $code,
            type  => SQL_INTEGER,
        },
        {
            param => $title,
            type  => SQL_VARCHAR,
        },
        {
            param => $description,
            type  => SQL_VARCHAR,
        },
    ];
    foreach my $param ( @{$params} ) {
        $sth->bind_param( ++$p_num, $param->{param}, $param->{type} );
        if ( $sth->err ) {
            $self->dberror(
                'report_product(): bind_param() failed: ' . $sth->errstr() );
            return 0;
        }
    }

    my $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror( 'report_product(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    return 1;
}

sub add_history {
    my ( $self, $user, $code ) = @_;

    my $sth = $dbh->prepare($DELETEHISTORYSTMT);
    if ( !defined $sth ) {
        $self->dberror( 'add_history(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    my $p_num;
    my $params = [
        {
            param => $user,
            type  => SQL_VARCHAR,
        },
        {
            param => $code,
            type  => SQL_INTEGER,
        },
    ];
    foreach my $param ( @{$params} ) {
        $sth->bind_param( ++$p_num, $param->{param}, $param->{type} );
        if ( $sth->err ) {
            $self->dberror(
                'add_history(): bind_param() failed: ' . $sth->errstr() );
            return 0;
        }
    }

    my $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror( 'add_history(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    $sth = $dbh->prepare($ADDHISTORYSTMT);
    if ( !defined $sth ) {
        $self->dberror( 'add_history(): prepare() failed: ' . $dbh->errstr() );
        return 0;
    }

    $p_num = 0;
    foreach my $param ( @{$params} ) {
        $sth->bind_param( ++$p_num, $param->{param}, $param->{type} );
        if ( $sth->err ) {
            $self->dberror(
                'add_history(): bind_param() failed: ' . $sth->errstr() );
            return 0;
        }
    }

    $rv = eval { $sth->execute; 1 };
    if ( $EVAL_ERROR || $sth->err || !$rv ) {
        $self->dberror( 'add_history(): execute() failed: ' . $sth->errstr );
        return 0;
    }

    return 1;
}

sub get_history {
    my ( $self, $user ) = @_;

    my $rows = $dbh->selectall_arrayref( $GETHISTORYSTMT, { RaiseError => 1 }, $user );

    if ( $dbh->err || !$rows ) {
        $self->dberror( 'get_history(): selectall_arrayref failed: '
              . $dbh->errstr );
        return 0;
    }

    my @results;
    foreach my $row ( @{$rows} ) {
        my ( $code, $nom, $description, $codebarre ) = @{$row};
        push @results,
          {
            map { Encode::decode( 'UTF-8', $_ ) } (
                code        => $code,
                nom         => $nom,
                description => $description,
                codebarre   => $codebarre,
            )
          };
    }

    $json->{nresults}      = @results;
    $json->{results}       = \@results;
    $json->{user}          = $user;

    return 1;
}



##############################################################################
# Score-related stuff

sub calculer_score {
    my ( $self, $code ) = @_;

    my $n                  = int score_negative($code);
    my $p                  = int score_positive($code);
    my $score_nutritionnel = $n - $p;
    my $classe;
    if ( $score_nutritionnel <= -1 ) {
        $classe = 'A';
    }
    elsif ( $score_nutritionnel < 2 ) {
        $classe = 'B';
    }
    elsif ( $score_nutritionnel < 10 ) {
        $classe = 'C';
    }
    elsif ( $score_nutritionnel < 18 ) {
        $classe = 'D';
    }
    else {
        $classe = 'E';
    }

    $json->{score} = $classe;
    return 1;
}

sub get_taux_by_ingredient_name {
    my ( $code, $ingredname ) = @_;

    my $row =
      $dbh->selectrow_arrayref( $GETTAUXBYINGREDNAMESTMT, { RaiseError => 1 },
        $code, $ingredname );

    if ($row) {
        return $row->[0];
    }

    return;
}

sub get_taux_by_ingredient_category {
    my ( $code, $ingredcat ) = @_;

    my $rows =
      $dbh->selectall_arrayref( $GETTAUXBYINGREDCATEGORYSTMT,
        { RaiseError => 1 },
        $code, $ingredcat );

    defined $rows or return;

    my $taux;
    foreach my $row ( @{$rows} ) {
        $taux += $row->[0];
    }

    return $taux;
}

sub score_positive {
    my ($code) = @_;

    return score_fruits_legumes($code) + score_fibres($code) +
      score_proteines($code);
}

sub score_negative {
    my ($code) = @_;

    return score_densite_energetique($code) +
      score_graisses_saturees($code) +
      score_sucres_simples($code) +
      score_sodium($code);
}

sub score_fruits_legumes {
    my ($code) = @_;
    my $taux;
    my @categories = qw( Fruits Légumes );
    foreach my $category (@categories) {
        $taux += ( get_taux_by_ingredient_category( $code, $category ) // 0 );
    }
    defined $taux or return 0;
    if ( $taux > 80 ) {
        return 5;
    }
    elsif ( $taux > 60 ) {
        return 2;
    }
    elsif ( $taux > 40 ) {
        return 1;
    }
    else {
        return 0;
    }
}

sub score_fibres {
    my ($code) = @_;
    my $name = 'Fibres';
    my $taux = get_taux_by_ingredient_name( $code, $name );
    defined $taux or return 0;
    if ( $taux > 4.7 ) {
        return 5;
    }
    elsif ( $taux > 3.7 ) {
        return 4;
    }
    elsif ( $taux > 2.8 ) {
        return 3;
    }
    elsif ( $taux > 1.9 ) {
        return 2;
    }
    elsif ( $taux > 0.9 ) {
        return 1;
    }
    else {
        return 0;
    }
}

sub score_proteines {
    my ($code) = @_;
    my $category = 'Protéines';
    my $taux = get_taux_by_ingredient_name( $code, $category );
    defined $taux or return 0;
    if ( $taux > 8.0 ) {
        return 5;
    }
    elsif ( $taux > 6.4 ) {
        return 4;
    }
    elsif ( $taux > 4.8 ) {
        return 3;
    }
    elsif ( $taux > 3.2 ) {
        return 2;
    }
    elsif ( $taux > 1.6 ) {
        return 1;
    }
    else {
        return 0;
    }
}

sub score_densite_energetique {
    my ($code) = @_;
    my $name = 'Densité énergétique';
    my $taux = get_taux_by_ingredient_name( $code, $name );
    defined $taux or return 0;
    if ( $taux > 3350 ) {
        return 10;
    }
    elsif ( $taux > 3015 ) {
        return 9;
    }
    elsif ( $taux > 2680 ) {
        return 8;
    }
    elsif ( $taux > 2345 ) {
        return 7;
    }
    elsif ( $taux > 2010 ) {
        return 6;
    }
    elsif ( $taux > 1675 ) {
        return 5;
    }
    elsif ( $taux > 1340 ) {
        return 4;
    }
    elsif ( $taux > 1005 ) {
        return 3;
    }
    elsif ( $taux > 670 ) {
        return 2;
    }
    elsif ( $taux > 355 ) {
        return 1;
    }
    else {
        return 0;
    }
}

sub score_graisses_saturees {
    my ($code) = @_;
    my $category = 'Graisses saturées';
    my $taux = get_taux_by_ingredient_category( $code, $category );
    defined $taux or return 0;
    if ( $taux < 2 ) {
        return 1;
    }
    elsif ( $taux < 10 ) {
        return $taux;
    }
    else {
        return 10;
    }
}

sub score_sucres_simples {
    my ($code) = @_;
    my $category = 'Sucres simples';
    my $taux = get_taux_by_ingredient_category( $code, $category );
    defined $taux or return 0;
    my @numbers = map { $_ * 4.5 } 1 .. 10;
    if ( $taux > $numbers[9] ) {
        return 10;
    }
    elsif ( $taux > $numbers[8] ) {
        return 9;
    }
    elsif ( $taux > $numbers[7] ) {
        return 8;
    }
    elsif ( $taux > $numbers[6] ) {
        return 7;
    }
    elsif ( $taux > $numbers[5] ) {
        return 6;
    }
    elsif ( $taux > $numbers[4] ) {
        return 5;
    }
    elsif ( $taux > $numbers[3] ) {
        return 4;
    }
    elsif ( $taux > $numbers[2] ) {
        return 3;
    }
    elsif ( $taux > $numbers[1] ) {
        return 2;
    }
    elsif ( $taux > $numbers[0] ) {
        return 1;
    }
    else {
        return 0;
    }
}

sub score_sodium {
    my ($code) = @_;
    my $name = 'Sodium';
    my $taux = get_taux_by_ingredient_name( $code, $name );
    defined $taux or return 0;
    my @numbers = map { $_ * 90 } 1 .. 10;
    if ( $taux > $numbers[9] ) {
        return 10;
    }
    elsif ( $taux > $numbers[8] ) {
        return 9;
    }
    elsif ( $taux > $numbers[7] ) {
        return 8;
    }
    elsif ( $taux > $numbers[6] ) {
        return 7;
    }
    elsif ( $taux > $numbers[5] ) {
        return 6;
    }
    elsif ( $taux > $numbers[4] ) {
        return 5;
    }
    elsif ( $taux > $numbers[3] ) {
        return 4;
    }
    elsif ( $taux > $numbers[2] ) {
        return 3;
    }
    elsif ( $taux > $numbers[1] ) {
        return 2;
    }
    elsif ( $taux > $numbers[0] ) {
        return 1;
    }
    else {
        return 0;
    }
}

1;

# vim: set ft=perl et sw=4 sts=4 ts=8 fenc=utf-8:

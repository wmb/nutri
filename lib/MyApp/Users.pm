package MyApp::Users;

use 5.026;
use strict;
use warnings ( FATAL => 'all' );

# Use UTF-8 encoding
use utf8;

# Use croak/carp instead of die/warn
# (cf. Effective Perl Programming, item 102)
use Carp;

use MyApp::DB;

# JSON
use JSON::MaybeXS;

# SHA512
use Digest::SHA 'sha512_hex';

# Satisfy perlcritic
use Readonly;
use English '-no_match_vars';

Readonly our $VERSION => '0.0.1';

my $json = undef;

sub new {
    my $class = shift;
    my $self  = {};
    $json = shift;
    if ( !defined $json ) {
        $json = { error => { error => JSON->false } };
    }

    return bless $self, $class;
}

sub checkuser {
    my ( $self, $user, $pass ) = @_;

    my $dbuser = $self->get_dbuser($user);

    $json->{user} = $dbuser;
    defined $dbuser or return 0;

    my $sha512digest = sha512_hex( $pass . q(:) . $dbuser->{email} );
    if ( $sha512digest eq $dbuser->{pass} ) {
        return 1;
    }

    # Remove sensitive information
    delete @{ $json->{user} }{qw(email pass)};

    return 0;
}

# Returns a hash ref to the user in question; or undef;
sub get_dbuser {
    my ( $self, $user ) = @_;

    my $db = MyApp::DB->new($json);
    $db->dbconnect(1) or return;
    my $dbuser = $db->finduser($user);
    $db->dbdisconnect;
    return $dbuser;
}

sub user_exists {
    my ( $self, $user, $pass ) = @_;
    my $j = undef;
    $j = { error => { error => JSON->false } };

    my $db = MyApp::DB->new($j);

    $db->dbconnect(1) or 0;
    my $dbuser = $db->finduser($user);
    $db->dbdisconnect;
    if ( defined $dbuser && $dbuser ) {
        return 1;
    }
    return 0;
}

sub adduser {
    my ( $self, $user, $pass, $email, $name ) = @_;
    Readonly my $MINIMUM_PASSWORD_LENGTH => 6;

    # Check password length
    if ( length($pass) < $MINIMUM_PASSWORD_LENGTH ) {
        $json->{error}->{error} = JSON->true;
        $json->{error}->{msg} =
"Le mot de passe doit être au moins $MINIMUM_PASSWORD_LENGTH caractères.";
        return 0;
    }

    # Check if the user already exists
    if ( $self->user_exists($user) ) {
        $json->{error}->{error} = JSON->true;
        $json->{error}->{msg}   = "L'utilisateur $user existe déjà.";
        return 0;
    }

    my $hashed_password = sha512_hex( $pass . q(:) . $email );

    if ( !$self->create_user( $user, $hashed_password, $email, $name ) ) {
        return 0;
    }

    return 1;
}

sub create_user {
    my ( $self, @credentials ) = @_;

    my $db = MyApp::DB->new($json);
    $db->dbconnect(0) or return 0;
    $db->createuser(@credentials) or return 0;
    $db->dbdisconnect;
    return 1;
}

1;

# vim: set ft=perl et sw=4 sts=4 ts=8 fenc=utf-8:

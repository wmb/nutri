#!/usr/bin/env perl

use 5.026;
use strict;
use warnings ( FATAL => 'all' );

# Automatically die after failed system calls
use autodie;

# Use UTF-8 encoding
use utf8;

# Use croak/carp instead of die/warn
# (cf. Effective Perl Programming, item 102)
use Carp;

# Mojolicious
use Mojolicious::Lite;

# Satisfy perlcritic
use Readonly;
use English '-no_match_vars';

use lib 'lib';
use MyApp::DB;
use MyApp::Users;

use JSON::MaybeXS;

Readonly our $VERSION => '0.0.1';

Readonly my %HTTP_CODES => (
    ok                     => 200,
    created                => 201,
    accepted               => 202,
    bad_request            => 400,
    unauthorized           => 401,
    not_found              => 404,
    forbidden              => 403,
    method_not_allowed     => 405,
    not_acceptable         => 406,
    unsupported_media_type => 415,
    service_unavailable    => 503,
    server_error           => 500,
    not_implemented        => 501,
);

# /search?q=some+product
any '/search' => sub {
    my $c    = shift;
    my $mydb = MyApp::DB->new;

    # Check if paramaters are there
    my $search = $c->param('q');
    if ( !defined $search ) {
        $mydb->dberror('Pas de parametre q!');
        return $c->render( json => $mydb->getjson );
    }

    $mydb->dbconnect(1) && $mydb->dbprodsearch($search) && $mydb->dbdisconnect;
    $c->render( json => $mydb->getjson );
};

sub newjson {
    return { error => { error => JSON->false } };
}

sub jsonerror {
    my ( $json, $msg ) = @_;
    defined $json or return;
    my $jsonerr = $json->{error};
    $msg //= 'An error occurred.';
    $jsonerr->{error} = JSON->true;
    $jsonerr->{msg}   = $msg;
    return $json;
}

# /login?user=foo&pass=bar
any '/login' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();
    my $users = MyApp::Users->new($json);

    if ( defined( my $user = $c->session('user') ) ) {
        $json->{info} = "You're already logged in as $user!";
        return $c->render( json => $json );
    }

    # Check paramaters
    my @notdefined;
    my $user = $c->param('user') or push @notdefined, 'user';
    my $pass = $c->param('pass') or push @notdefined, 'pass';

    if (@notdefined) {
        jsonerror( $json,
                ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.' );
        return $c->render( json => $json );
    }

    if ( !$users->checkuser( $user, $pass ) ) {

        #delete $json->{user};
        jsonerror( $json, 'Wrong username or password.' );
        return $c->render( json => $json );
    }

    $c->session( 'user'       => $json->{user}->{pseudo} );
    $c->session( 'user.email' => $json->{user}->{email} );
    $c->session( 'user.nom'   => $json->{user}->{nom} );
    $c->render( json => $json );
};

# /logout
any '/logout' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    if ( defined( my $user = $c->session('user') ) ) {
        $json->{info} = "I logged you out, $user.";
    }
    else {
        $json->{info} = q(You weren't logged in.);
    }

    $c->session( expires => 1 );

    $c->render( json => $json );
};

# /adduser?user=someuser&pass=mystrongpassword&name=Some+User&mail=some.user%40example.com
any '/adduser' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    if ( defined( my $user = $c->session('user') ) ) {
        jsonerror( $json,
                "You are logged in as $user."
              . ' Please log out before creating an account.' );
        return $c->render( json => $json );
    }

    # Check paramaters
    my @notdefined;
    my $user = $c->param('user') or push @notdefined, 'user';
    my $pass = $c->param('pass') or push @notdefined, 'pass';
    my $mail = $c->param('mail') or push @notdefined, 'mail';
    my $name = $c->param('name') or push @notdefined, 'name';

    if (@notdefined) {
        jsonerror( $json,
                ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.' );
        return $c->render( json => $json );
    }

    my $users = MyApp::Users->new($json);
    if ( !$users->adduser( $user, $pass, $mail, $name ) ) {
        return $c->render( json => $json );
    }

    $c->render( json => $json );
};

# Requires authentication
# /addproduct
post '/addproduct' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    if ( !defined( $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my $input = $c->req->json;

    my $db = MyApp::DB->new($json);
    my $rv = eval {
        $db->dbconnect(0);
        $db->addproduct($input);
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || !$rv ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

# Requires authentication
# /addcatprod?name=cat1&desc=description
post '/addcatprod' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    if ( !defined( $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $name = $c->param('name') or push @notdefined, 'name';
    my $desc = $c->param('desc') or push @notdefined, 'desc';

    if (@notdefined) {
        jsonerror( $json,
                ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.' );
        return $c->render( json => $json );
    }

    my $db = MyApp::DB->new($json);

    $db->dbconnect(0) && $db->addcatprod( $name, $desc ) && $db->dbdisconnect;

    $c->render( json => $json );
};

# Requires authentication
# /addcatingred?name=cat1&desc=description
post '/addcatingred' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    if ( !defined( $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $name = $c->param('name') or push @notdefined, 'name';
    my $desc = $c->param('desc') or push @notdefined, 'desc';

    if (@notdefined) {
        jsonerror( $json,
                ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.' );
        return $c->render( json => $json );
    }

    my $db = MyApp::DB->new($json);

    $db->dbconnect(0) && $db->addcatingred( $name, $desc ) && $db->dbdisconnect;

    $c->render( json => $json );
};

# '/getcatprods?name=some+category&exact=1'
get '/getcatprods' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    my $name  = $c->param('name')  // q();
    my $exact = $c->param('exact') // 0;

    # if exact is true, only return results that match perfectly.

    my $db = MyApp::DB->new($json);

    $db->dbconnect(1)
      && ( $exact ? $db->getcatprod($name) : $db->getcatprods($name) )
      && $db->dbdisconnect;

    $c->render( json => $json );
};

# Get all products that match a certain category
get '/products_by_catprod' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    my $category = $c->param('category');
    my $exact    = $c->param('exact') // 0;

    if ( !defined $category ) {
        jsonerror( $json, 'Please specify a category.' );
        return $c->render( json => $json );
    }

    my $db = MyApp::DB->new($json);

    $db->dbconnect(1)
      && $db->products_by_catprod( $category, $exact )
      && $db->dbdisconnect;

    $c->render( json => $json );
};

get '/product_categories' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    my $code = $c->param('code');

    if ( !defined $code ) {
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{bad_request} )
                  . ': Please specify a product code.'
            )
        );
    }

    if ( $code !~ m/\A  0 | [1-9]\d* \z/mosx ) {
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{bad_request} )
                  . ': Product code should be a number.'
            )
        );
    }

    my $db = MyApp::DB->new($json);

    $db->dbconnect(1)
      && $db->product_categories($code)
      && $db->dbdisconnect;

    $c->render( json => $json );
};

# Find product by barcode
# '/barcode?type=EAN13&code=123456789012'
get '/barcode' => sub {
    my $c    = shift;
    my $json = newjson();

    my @notdefined;
    my $type = $c->param('type') or push @notdefined, 'type';
    my $code = $c->param('code') or push @notdefined, 'code';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new;

    my $rv = eval {
             $db->dbconnect(1)
          && $db->prod_by_barcode( $type, $code )
          && $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $db->getjson );
};

# Get a list of available product categories
get '/list_product_categories' => sub {
    my $c    = shift;
    my $json = newjson();
    my $db   = MyApp::DB->new($json);

    my $rv = eval {
        $db->dbconnect(1);
        $db->listcategories('product');
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

# Get a list of available ingredient categories
get '/list_ingredient_categories' => sub {
    my $c    = shift;
    my $json = newjson();
    my $db   = MyApp::DB->new($json);

    my $rv = eval {
        $db->dbconnect(1);
        $db->listcategories('ingredient');
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

# Get a list of available ingredients
get '/list_ingredients' => sub {
    my $c    = shift;
    my $json = newjson();
    my $db   = MyApp::DB->new($json);

    my $rv = eval {
        $db->dbconnect(1);
        $db->listingredients;
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

# Add a category to a product
post '/add_product_category' => sub {
    my $c    = shift;
    my $json = newjson();

    if ( !defined( $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $product  = $c->param('product')  or push @notdefined, 'product';
    my $category = $c->param('category') or push @notdefined, 'category';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new($json);

    my $rv = eval {
        $db->dbconnect(0);
        $db->add_product_category( $product, $category );
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

# Add an ingredient to a product
post '/add_product_ingredient' => sub {
    my $c    = shift;
    my $json = newjson();

    if ( !defined( $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $product    = $c->param('product')    or push @notdefined, 'product';
    my $ingredient = $c->param('ingredient') or push @notdefined, 'ingredient';
    my $taux       = $c->param('taux')       or push @notdefined, 'taux';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new($json);

    my $rv = eval {
        $db->dbconnect(0);
        $db->add_product_ingredient( $product, $ingredient, $taux );
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

# Add an additive to a product
post '/add_product_additive' => sub {
    my $c    = shift;
    my $json = newjson();

    if ( !defined( $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $product  = $c->param('product')  or push @notdefined, 'product';
    my $additive = $c->param('additive') or push @notdefined, 'additive';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new($json);

    my $rv = eval {
        $db->dbconnect(0);
        $db->add_product_additive( $product, $additive );
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

# Create a new ingredient
post '/create_ingredient' => sub {
    my $c    = shift;
    my $json = newjson();

    if ( !defined( $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $name     = $c->param('name')     or push @notdefined, 'name';
    my $desc     = $c->param('desc')     or push @notdefined, 'desc';
    my $category = $c->param('category') or push @notdefined, 'category';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new($json);

    my $rv = eval {
        $db->dbconnect(0);
        $db->create_ingredient( $name, $desc, $category );
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

# '/nutriscore?code=1234'
get '/nutriscore' => sub {
    my $c    = shift;
    my $json = newjson();

    my @notdefined;
    my $code = $c->param('code') or push @notdefined, 'code';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new;

    my $rv = eval {
             $db->dbconnect(1)
          && $db->calculer_score($code)
          && $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $db->getjson );
};

# '/product_ingredients?code=1234'
get '/product_ingredients' => sub {
    my $c    = shift;
    my $json = newjson();

    my @notdefined;
    my $code = $c->param('code') or push @notdefined, 'code';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new;

    my $rv = eval {
             $db->dbconnect(1)
          && $db->product_ingredients($code)
          && $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $db->getjson );
};

# '/product_additives?code=1234'
get '/product_additives' => sub {
    my $c    = shift;
    my $json = newjson();

    my @notdefined;
    my $code = $c->param('code') or push @notdefined, 'code';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new;

    my $rv = eval {
             $db->dbconnect(1)
          && $db->product_additives($code)
          && $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $db->getjson );
};

# '/product_categories?code=1234'
get '/product_categories' => sub {
    my $c    = shift;
    my $json = newjson();

    my @notdefined;
    my $code = $c->param('code') or push @notdefined, 'code';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new;

    my $rv = eval {
             $db->dbconnect(1)
          && $db->product_categories($code)
          && $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $db->getjson );
};

get '/is_starred' => sub {
    my $c    = shift;
    my $json = newjson();

    my $user;

    if ( !defined( $user = $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $code = $c->param('code') or push @notdefined, 'code';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new;

    my $rv = eval {
             $db->dbconnect(1)
          && $db->is_starred( $code, $user )
          && $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $db->getjson );
};

get '/star_product' => sub {
    my $c    = shift;
    my $json = newjson();

    my $user;

    if ( !defined( $user = $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $code = $c->param('code') or push @notdefined, 'code';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new;

    my $rv = eval {
             $db->dbconnect(0)
          && $db->star_product( $code, $user )
          && $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $db->getjson );
};

get '/unstar_product' => sub {
    my $c    = shift;
    my $json = newjson();

    my $user;

    if ( !defined( $user = $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $code = $c->param('code') or push @notdefined, 'code';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new;

    my $rv = eval {
             $db->dbconnect(0)
          && $db->unstar_product( $code, $user )
          && $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $db->getjson );
};

get '/list_starred' => sub {
    my $c    = shift;
    my $json = newjson();

    my $user;

    if ( !defined( $user = $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my $db = MyApp::DB->new;

    my $rv = eval {
             $db->dbconnect(0)
          && $db->list_starred($user)
          && $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || $rv != 1 ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $db->getjson );
};

post '/report_product' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    my $user;

    if ( !defined( $user = $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $code  = $c->param('code')  or push @notdefined, 'code';
    my $title = $c->param('title') or push @notdefined, 'title';
    my $description = $c->param('description')
      or push @notdefined, 'description';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new($json);
    my $rv = eval {
        $db->dbconnect(0);
        $db->report_product( $user, $code, $title, $description );
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || !$rv ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

post '/add_history' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    my $user;

    if ( !defined( $user = $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my @notdefined;
    my $code  = $c->param('code')  or push @notdefined, 'code';

    if (@notdefined) {
        jsonerror(
            $json,
            join ': ',
            $c->res->default_message( $HTTP_CODES{bad_request} ),
            ( join ' and ', @notdefined )
              . ( $#notdefined ? ' are' : ' is' )
              . ' not specified.'
        );
        return $c->render(
            status => $HTTP_CODES{bad_request},
            json   => $json
        );
    }

    my $db = MyApp::DB->new($json);
    my $rv = eval {
        $db->dbconnect(0);
        $db->add_history( $user, $code );
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || !$rv ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

get '/get_history' => sub {
    my $c    = shift;
    my $json = undef;
    $json = newjson();

    my $user;

    if ( !defined( $user = $c->session('user') ) ) {
        return $c->render(
            status => $HTTP_CODES{unauthorized},
            json   => jsonerror(
                $json,
                $c->res->default_message( $HTTP_CODES{unauthorized} )
                  . ': You need to authenticate'
            )
        );
    }

    my $db = MyApp::DB->new($json);
    my $rv = eval {
        $db->dbconnect(1);
        $db->get_history($user);
        $db->dbdisconnect;
        1;
    };
    if ( $EVAL_ERROR || !$rv ) {
        jsonerror(
            $json, join ': ',
            $c->res->default_message( $HTTP_CODES{server_error} ),
            $json->{error}->{msg} // $EVAL_ERROR
        );
        return $c->render(
            status => $HTTP_CODES{server_error},
            json   => $json
        );
    }

    $c->render( json => $json );
};

eval {
    plugin( 'DBViewer', dsn => MyApp::DB->new->dbgetdsn );
    1;
}
  or say 'Install Mojolicious::Plugin::DBViewer'
  or carp "say failed: $OS_ERROR";

app->start();

# vim: set et ft=perl et sw=4 sts=4 ts=8 fenc=utf-8:

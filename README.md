# Nutri

## Comment installer

### Windows

1. Installer [Strawberry Perl](http://strawberyperl.com)
2. Ouvrir `cmd.exe` comme administrateur
3. Installer [`cpanm`](http://search.cpan.org/perldoc/App::cpanminus):

            perl -MCPAN -e shell
            cpan> install App::cpanminus
            ...
            cpan> q

4. Installer les modules suivants en utilisant `cpanm`:

            cpanm Mojolicious DBI DBD::SQLite Digest::SHA Cpanel::JSON::XS JSON::MaybeXS

5. Lancer l'application:

            cd nutri
            morbo -m development -l http://127.0.0.1:8080/ nutri.pl

    Le serveur sera disponible à l'adresse `http://127.0.0.1:8080/`

### GNU/Linux

Perl est préinstallé sur toutes les distributions de GNU/Linux que je connais,
vous devez juste installer les modules.

il est préférable d'utiliser le package manager de votre distribution
pour installer ces modules s'il y a des packages pour eux.
Consultez la documentation de votre système d'exploitation pour plus
d'informations.

Si votre distribution n'a pas de packages pour certains modules, vous pouvez
toujours les installer en utilisant `cpan` ou `cpanm`.

Les modules que vous devez installer sont les suivant:

- [`Mojolicious`](http://mojolicious.org)
- [`DBD::SQLite`](http://search.cpan.org/dist/DBD-SQLite)
- [`JSON::MaybeXS`](http://search.cpan.org/dist/JSON-MaybeXS)
- [`Cpanel::JSON::XS`](http://search.cpan.org/dist/Cpanel-JSON-XS)
- [`Digest::SHA`](http://search.cpan.org/dist/Digest-SHA)
